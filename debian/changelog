alex4 (1.1-10) unstable; urgency=low

  [ Alexandre Detiste ]
  * drop non-free alex4-data, move package to contrib (Closes: #1035043)
  * Upgrade to debhelper-compat 13

  [ Pino Toscano ]
  * Drop menu file, since alex4 already provides a .desktop file.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use versioned copyright format URI.
  * Use secure URI in Homepage field.
  * Update standards version to 4.6.2, no changes needed.

 -- Alexandre Detiste <tchet@debian.org>  Fri, 01 Dec 2023 18:36:21 +0100

alex4 (1.1-9) unstable; urgency=medium

  * Team upload.
  * Upgrade to debhelper-compat 12
  * Upgrade to Standards version 4.5.0 (No changes required)
  * Reference the salsa repo in Vcs-* fields, and make canonical
  * Add patch fix_errors_with_multiple_definitions.patch
    (Closes: #956988)
  * Add description and author information to
    fix_building_with_fix_version.patch

 -- Andreas Rönnquist <gusnan@debian.org>  Fri, 17 Apr 2020 17:54:27 +0200

alex4 (1.1-8) unstable; urgency=medium

  * Team upload.
  * Add patch to build with fix*-functions, for Allegros fixed math,
    instead of f*-functions which conflict with newer glibc.
    (Closes: #916033)

 -- Andreas Rönnquist <gusnan@debian.org>  Sun, 09 Dec 2018 21:01:39 +0100

alex4 (1.1-7) unstable; urgency=medium

  * Team upload.

  [ Markus Koschany ]
  * Use compat level 10.
  * Declare compliance with Debian Policy 3.9.8.
  * Vcs-Browser: Use https.
  * Drop dpkg-dev build-dependency.
  * alex4.desktop: Add keywords and a comment in German.

  [ Jackson Doak ]
  * Drop build-dependency on hardening-wrapper. (Closes: #836750)

 -- Markus Koschany <apo@debian.org>  Thu, 15 Sep 2016 21:05:33 +0200

alex4 (1.1-6) unstable; urgency=low

  * Team upload.
  * Add alex4.ini so that sound works (LP: #560664)
  * Add myself to uploaders
  * Build against liballegro4-dev instead (Closes: #710598)
  * Use the same longtitle in the Debian menu as the desktop comment
  * Use the canonical Vcs-* fields
  * Bump Standards-Version to 3.9.4, no changes needed

 -- Paul Wise <pabs@debian.org>  Sun, 09 Jun 2013 12:22:28 +0800

alex4 (1.1-5) unstable; urgency=low

  * Team upload.
  * Include defs.h *after* particle.h, thus not redifining __unused from
    glibc's bits/stat.h
    Closes: #624884
  * Standards-Version: 3.9.2

 -- Evgeni Golov <evgeni@debian.org>  Sat, 07 May 2011 15:34:08 +0200

alex4 (1.1-4) unstable; urgency=low

  * Update the unix-port patch a bit:
    - place the libraries at the end of the linker invocation line to
      fix the build on Ubuntu natty with the binutils-gold linker.
      I couldn't reproduce the FTBFS on Debian with
      binutils-gold-2.21.0.20110302-1, but it seems a really sensible
      change to make anyway :)
      Closes: #617465
    - put the aldumb library before the Allegro libraries that it
      depends on
    - honor CPPFLAGS, CFLAGS and LDFLAGS
  * Convert to the 3.0 (quilt) source format.
  * Add misc:Depends to the alex4-data package just in case.
  * Expand the alex4-data package's long description.
  * Bump the debhelper compatibility level to 8 and minimize the rules file
    using debhelper override targets.
  * Bump Standards-Version to 3.9.1 with no changes.
  * Use dpkg-buildflags to obtain the default values for CPPFLAGS, CFLAGS
    and LDFLAGS.
  * Convert the copyright file to the DEP 5 candidate format and add my
    copyright notice on the debian/* packaging.
  * Convert all patch file headers to the DEP 3 format.
  * Build with -Werror if the non-standard "werror" build option is set.
  * Add the compiler-warnings patch to fix some, well, compiler warnings.
  * Harden the build unless the "nohardening" build option is set.
  * Add myself to the list of uploaders.

 -- Peter Pentchev <roam@ringlet.net>  Wed, 09 Mar 2011 22:18:03 +0200

alex4 (1.1-3) unstable; urgency=low

  [ Paul Wise ]
  * Add a watch file explaining the upstream status

  [ Peter De Wachter ]
  * Honor the HOME environment variable. (Closes: #507950)
  * Applied a patch to bring down the game's CPU usage to a more reasonable
    level. This limits the frame rate to 50Hz (the speed of the game logic).
    This also fixes a bug that made the level selection screen unresponsive
    on some systems.
  * Bump Standards-Version to 3.8.1:
     - Renamed 'configure' target to 'patch'.
  * Rename from-fedora.patch to unix-port.patch
  * Refresh debian/copyright
  * rules: clean up

  [ Gonéri Le Bouder ]
  * Complet Peter changelog part

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Sun, 07 Jun 2009 16:43:48 +0200

alex4 (1.1-2) unstable; urgency=low

  * Build-Depend on liballegro4.2-dev >= 4.2.2-2
     - which has a versioned shlibs file (Closes: #467526)
     - allegro-config no longer tells us to link against unneeded libraries.

 -- Peter De Wachter <pdewacht@gmail.com>  Wed, 19 Mar 2008 00:03:46 +0100

alex4 (1.1-1) unstable; urgency=low

  * Initial release (Closes: #463033)

 -- Peter De Wachter <pdewacht@gmail.com>  Sat, 19 Jan 2008 19:26:51 +0100
